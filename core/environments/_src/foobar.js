// use kDeployer's internal minification or closure compiler to minimise this file.

function getFirstImage(html) {
    //@ example image tag:
    //   <img alt=\"GOPR9503_1450461256625_high.JPG\" src=\"http://1.bp.blogspot.com/-ZG8_M2DKVkQ/VnXJxT2vsSI/AAAAAAAA4wU/uxBYtiigRSQ/s1000/GOPR9503_1450461256625_high.JPG\" />
    let re      = /<img[^>]+src=\\?"?([^"\s]+)\\?"?[^>]*\/>/g;
    let results = re.exec(html);
    let img     = "";
    if (results) img = results[1];
    // remove "s72-c/" from the image urls - fix for blogger's reduced thumbnail sizes (https://github.com/klevstul/foreNeno/issues/393)
    img = img.replace('s72-c/','');
    return img;
}
