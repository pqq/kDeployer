# -------------------------------------------------------------------------------------
# filename: kClosureCompiler.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  2017.03.29
# -------------------------------------------------------------------------------------

# --- requirements --------------------------------------------------------------------
# LWP::Protocol::https  :   https://metacpan.org/pod/LWP::Protocol::https
# -------------------------------------------------------------------------------------

# --- documentation -------------------------------------------------------------------
# http response         :   http://search.cpan.org/~ether/HTTP-Message-6.11/lib/HTTP/Response.pm
#                       :   http://lwp.interglacial.com/ch03_05.htm
# http request common   :   http://search.cpan.org/~ether/HTTP-Message-6.11/lib/HTTP/Request/Common.pm
# user agent            :   http://search.cpan.org/~oalders/libwww-perl-6.24/lib/LWP/UserAgent.pm
# closure compiler      :   https://developers.google.com/closure/compiler/docs/api-ref
# -------------------------------------------------------------------------------------


# -------------------------------------------------------------------------------------
# package setup
# -------------------------------------------------------------------------------------
package kClosureCompiler;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	kCC_compile
);


# -------------------------------------------------------------------------------------
# use
# -------------------------------------------------------------------------------------
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use HTTP::Response;
use strict;


# -------------------------------------------------------------------------------------
# for testing purposes only : uncomment and run `perl kClosureCompiler.pm`
# -------------------------------------------------------------------------------------
#my @result = kCC_compile('if (1===2) { alert("i will never say hello to the world."); } // This comment should be stripped');
#my @result = kCC_compile('//# sourceMappingURL=whatever');
#print "status: " . $result[0] . "\n";
#print "result: " . $result[1];


# -------------------------------------------------------------------------------------
# sub routines
# -------------------------------------------------------------------------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# compiles javascript, returns the compiled result
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCC_compile {
    my $js_code = $_[0];

    my @returnArray;
    my $url = "https://closure-compiler.appspot.com/compile";
    my $userAgent = LWP::UserAgent->new();
    my $request = POST $url, [
        js_code => $js_code,
        compilation_level => 'WHITESPACE_ONLY', 		# {WHITESPACE_ONLY, SIMPLE_OPTIMIZATIONS, ADVANCED_OPTIMIZATIONS}
		#language => 'ECMASCRIPT_2017',					# https://github.com/google/closure-compiler/blob/f087f70c6e4b595f9a8f8067691de2fa3c8a9e03/src/com/google/javascript/jscomp/CompilerOptions.java#L3048-L3093
        output_format => 'text',            			# {xml,json,text}
        output_info => 'compiled_code',
		#output_info => 'statistics',        			# for getting statistics (for debugging purposes)
        output_info => 'errors'
    ];

    my $response = $userAgent->request($request);
    my $httpStatusCode = $response->code;
    my $isSuccess = $response->is_success;

    if ($response->is_success) {
        if ($response->decoded_content =~ m/Input_[^\/n]+ERROR\s-\s/){
            $returnArray[0] = "ERROR";
        } else {
            $returnArray[0] = "SUCCESS";
        }
        $returnArray[1] = $response->decoded_content;
    } else {
        $returnArray[0] = "ERROR";
        $returnArray[1] = $response->status_line;
    }

    return @returnArray;
}
