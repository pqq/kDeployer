ECHO OFF
:: CLS

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: what:        copy code to kDeployer source folder - this has to be done as first step, when building a new environment
:: author:      frode klevstul (frode at klevstul dot com)
:: started:     may 2016
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set project specific directory (full path)
:: - - - - -
SET PROJECTSPECIFICKDEPLOYERDIR=R:\googleDrive\projects\_gitHub\foreNeno\kDeployer

:: - - - - -
:: set relative (to project dir) paths to source folders, where we will copy content from
:: - - - - -
SET SOURCE_DIR_1=..\webApp\src
SET SOURCE_DIR_2=..\cgi
SET SOURCE_DIR_3=..\database
SET SOURCE_DIR_4=..\..\foreNeno.cfg


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: GENERIC CODE, THAT MOST LIKELY IS SIMILAR IN BETWEEN PROJECTS, HENCE NOT MUCH EDITING OF THE CODE BELOW SHOULD BE NEEDED
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set environment's source dir (relative, the the project dir, path)
:: - - - - -
SET ENVIRONMENTSSOURCEDIR=environments\_src

:: - - - - -
::  set current dir (as "oldDir")
:: - - - - -
SET oldDir=%CD%

:: - - - - -
:: set target directory, where the files will be copied to (relative path, to the project folder)
:: - - - - -
SET TARGET_DIR=%ENVIRONMENTSSOURCEDIR%\

:: - - - - -
:: change directory to the project specific directory
:: - - - - -
CD /D %PROJECTSPECIFICKDEPLOYERDIR%

:: - - - - -
:: delete old content from source dir
:: - - - - -
DEL /q "%TARGET_DIR%\*"
FOR /D %%p IN ("%TARGET_DIR%\*.*") DO rmdir "%%p" /s /q

:: - - - - -
:: copy relevant content into source dirs (note: this command is executed inside the project specific directory)
:: - - - - -
xcopy /E/D/Y /EXCLUDE:project_excludedfileslist.txt %SOURCE_DIR_1%\* %TARGET_DIR%
xcopy /E/D/Y /EXCLUDE:project_excludedfileslist.txt %SOURCE_DIR_2%\* %TARGET_DIR%
xcopy /E/D/Y /EXCLUDE:project_excludedfileslist.txt %SOURCE_DIR_3%\* %TARGET_DIR%
xcopy /E/D/Y /EXCLUDE:project_excludedfileslist.txt %SOURCE_DIR_4%\* %TARGET_DIR%

:: - - - - -
:: move back to old directory
:: - - - - -
CD /D %oldDir%
