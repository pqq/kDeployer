ECHO OFF
::CLS

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: what:        builds an environment from source (copyToSourceFolder.bat needs to have been executed first)
:: author:      frode burdal klevstul (frode at klevstul dot com)
:: started:     may 2016
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set project specific directory (full path)
:: - - - - -
SET PROJECTSPECIFICKDEPLOYERDIR=R:\googleDrive\projects\_gitHub\foreNeno\kDeployer

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: GENERIC CODE, THAT MOST LIKELY IS SIMILAR IN BETWEEN PROJECTS, HENCE NOT MUCH EDITING OF THE CODE BELOW SHOULD BE NEEDED
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set/get command line arguments
:: - - - - -
SET ENV=%1

:: - - - - -
::  set current dir (as "oldDir")
:: - - - - -
SET oldDir=%CD%

:: - - - - -
:: set source directory, where the files will be copied from (relative path, to the project folder)
:: - - - - -
SET SOURCE_DIR=environments\_src

:: - - - - -
:: set temporary directory, where the files will be copied to, on their way to the final build dir (relative path, to the project folder)
:: - - - - -
SET TMP_DIR=environments\_tmp

:: - - - - -
:: set target directory, where the files will be copied to (relative path, to the project folder)
:: - - - - -
SET TARGET_DIR=environments\%ENV%

:: - - - - -
:: change directory to the project specific directory
:: - - - - -
CD /D %PROJECTSPECIFICKDEPLOYERDIR%

IF NOT EXIST %PROJECTSPECIFICKDEPLOYERDIR%\%TARGET_DIR% (
    ECHO unable to locate the directory %PROJECTSPECIFICKDEPLOYERDIR%\%TARGET_DIR%
    GOTO :THE_END
)

:: - - - - -
:: delete content from temp dir
:: - - - - -
DEL /q "%TMP_DIR%\*"
FOR /D %%p IN ("%TMP_DIR%\*.*") DO rmdir "%%p" /s /q

:: - - - - -
:: delete content from target dir
:: - - - - -
DEL /q "%TARGET_DIR%\*"
FOR /D %%p IN ("%TARGET_DIR%\*.*") DO rmdir "%%p" /s /q

:: - - - - -
:: copy relevant content from source dir, to temp dir and selected environment (note: this command is executed inside the project specific directory)
:: note: kDeployer will overwrite several files ending up in the environment dir, as the build process will happen below.
:: - - - - -
xcopy /E/D/Y %SOURCE_DIR%\* %TMP_DIR%\
ECHO ... to "%TMP_DIR%"
xcopy /E/D/Y /EXCLUDE:project_buildfilesexclude.txt %SOURCE_DIR%\* %TARGET_DIR%\
ECHO ... to "%TARGET_DIR%"

:: - - - - -
:: change directory to the source directory
:: - - - - -
CD /D %SOURCE_DIR%

:: - - - - -
:: loop through all files in current directory, and deploy them
:: - - - - -
ECHO looping through all files, and running "kDeployer.pl"
for /r %%i in (*) do (
    perl %PROJECTSPECIFICKDEPLOYERDIR%\kDeployer.pl -min js,json,css -rc ico,map,gif,png,txt,jpg -e %ENV% -full -f %%i
)

:: - - - - -
:: end label, used for jumping to the end
:: - - - - -
:THE_END

:: - - - - -
:: move back to old directory
:: - - - - -
CD /D %oldDir%
