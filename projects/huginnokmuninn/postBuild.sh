#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        run post operations after a build has been done
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set the directory path to where the project has been built
# - - - - -
kdeployer_environments_dir='/media/dataDrive/Dropbox/gitlab/midgard/environments/'


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# GENERIC CODE, THAT MOST LIKELY IS SIMILAR IN BETWEEN PROJECTS, HENCE NOT MUCH EDITING OF THE CODE BELOW SHOULD BE NEEDED
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set/get command line arguments
# - - - - -
if [ $# -ne 1 ]; then
    echo $0: usage: postBuild.sh [environment]
    exit 1
fi

environment=$1

# - - - - -
#  set log directory
# - - - - -
log_dir=$kdeployer_environments_dir$environment'/log/'

# - - - - -
#  change access rights on log files
# - - - - -
chmod o+w $log_dir/*.log

# - - - - -
#  empty all log files (no need to pass on dev log data)
# - - - - -
for dir in $log_dir*.log ; do
    echo '' > $dir
done
