#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        build an environment from source (copyToSourceFolder.sh needs to have been executed first)
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     jan 2019
#
# requires: trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set project specific kDeployer directory (full path)
# - - - - -
project_specific_kdeployer_dir='/media/dataDrive/Dropbox/gitlab/midgard/kDep/'

# - - - - -
# set path to source folder (full path)
# - - - - -
source_code_dir='/media/dataDrive/Dropbox/gitlab/midgard/src/'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# GENERIC CODE, THAT MOST LIKELY IS SIMILAR IN BETWEEN PROJECTS, HENCE NOT MUCH EDITING OF THE CODE BELOW SHOULD BE NEEDED
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# kDeployer script name
# - - - - -
kDeployer='kDeployer.pl'

# - - - - -
# set/get command line arguments
# - - - - -
if [ $# -ne 1 ]; then
    echo $0: usage: environmentBuilder.sh [environment]
    exit 1
fi

environment=$1

# - - - - -
#  set current dir (as "oldDir")
# - - - - -
old_dir=`pwd`

# - - - - -
# set temporary directory, where the files will be copied to, on their way to the final build dir (relative path, to the project folder) - needed to make sure all dirs are created so kDeployer will work
# - - - - -
tmp_dir='../tmp/'

# - - - - -
# set target directory, where the files will be copied to (relative path, to the project folder)
# - - - - -
target_dir='../environments/'$environment

# - - - - -
# change directory to the project specific directory
# - - - - -
cd $project_specific_kdeployer_dir

# - - - - -
# delete content from target dir
# - - - - -
trash-put -r $target_dir

# - - - - -
# copy folder structure (all folders) from source dir to tmp dir and selected environment
# (note: this command is executed from inside the project specific directory)
# - - - - -
rsync -a $source_code_dir $tmp_dir --include='*/' --exclude='*'
rsync -a $source_code_dir $target_dir --include='*/' --exclude='*'

# - - - - -
# loop through all files in current directory, and deploy them
# - - - - -
echo looping through all files, and running "kDeployer.pl"

for f in $(find $source_code_dir -name '*' -type f -or -name '*.someFileType'); do
    if ! [[ $f == "./" ]]; then
        # kDeployer with minification:
        perl $project_specific_kdeployer_dir$kDeployer -min js,json,css -rc ico,map,gif,png,txt,jpg,pickle,log,xml -e $environment -full -f $f;
        # kDeployer without minification:
        #perl $project_specific_kdeployer_dir$kDeployer -rc ico,map,gif,png,txt,jpg -e $environment -full -f $f;
    fi
done

# - - - - -
# delete content from temp dir
# - - - - -
trash-put -r $tmp_dir

# - - - - -
# move back to old directory (strictly not neccessary on linux)
# - - - - -
cd $old_dir
