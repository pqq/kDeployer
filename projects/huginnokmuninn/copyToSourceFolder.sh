#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        copy code to kDeployer source folder - this has to be done as first step, when building a new environment
# author:      frode klevstul (frode at klevstul dot com)
# started:     jan 2019
#
# requires: trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set project specific kDeployer directory (full path)
# - - - - -
project_specific_kdeployer_dir='/media/dataDrive/Dropbox/gitlab/midgard/kDep/'

# - - - - -
# set path to source folders, where we will copy content from (full path)
# - - - - -
copy_from_dir_1='/media/dataDrive/Dropbox/gitlab/muninn/src/'
copy_from_dir_2='/media/dataDrive/Dropbox/gitlab/huginn/src/'
copy_from_dir_3='/media/dataDrive/Dropbox/gitlab/muninn/database'
copy_from_dir_4='/media/dataDrive/Dropbox/gitlab/muninn/log'

# - - - - -
# set relative (to target_dir) path to target folders for each source of the directories above
# - - - - -
target_rel_dir_1='api/'
target_rel_dir_2=''
target_rel_dir_3=''
target_rel_dir_4=''

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# GENERIC CODE, THAT MOST LIKELY IS SIMILAR IN BETWEEN PROJECTS, HENCE NOT MUCH EDITING OF THE CODE BELOW SHOULD BE NEEDED
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set environment's source code dir (relative, the the project dir, path)
# - - - - -
source_code_dir='../src/'

# - - - - -
#  set current dir (as "oldDir")
# - - - - -
old_dir=`pwd`

# - - - - -
# set target directory, where the files will be copied to (relative path, to the project folder)
# - - - - -
target_dir=$source_code_dir

# - - - - -
# change directory to the project specific directory
# - - - - -
cd $project_specific_kdeployer_dir

# - - - - -
# delete old content from source dir ()
# - - - - -
trash-put -r $target_dir/*

# - - - - -
# copy relevant content into source dirs (note: this command is executed inside the project specific directory)
# - - - - -

# rsync av --progress sourcefolder /destinationfolder --exclude thefoldertoexclude
#   https://sites.google.com/site/rsync2u/home/rsync-tutorial/the-exclude-from-option
# use "-n" for dry-run/test
rsync -a $copy_from_dir_1 $target_dir$target_rel_dir_1 --exclude-from=$old_dir/project_sourcecopyexcludelist.txt
rsync -a $copy_from_dir_2 $target_dir$target_rel_dir_2 --exclude-from=$old_dir/project_sourcecopyexcludelist.txt
rsync -a $copy_from_dir_3 $target_dir$target_rel_dir_3 --exclude-from=$old_dir/project_sourcecopyexcludelist.txt
rsync -a $copy_from_dir_4 $target_dir$target_rel_dir_4 --exclude-from=$old_dir/project_sourcecopyexcludelist.txt

# - - - - -
# move back to old directory (strictly not neccessary on linux)
# - - - - -
cd $old_dir
