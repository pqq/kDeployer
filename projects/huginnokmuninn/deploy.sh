#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        deploy script, to deploy kDeployer into a project
# author:      frode klevstul (frode at klevstul dot com)
# started:     jan 2019
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - -
# set base target dir - full path to project specific kDeployer folder
# - - - - -
target_dir='/media/dataDrive/Dropbox/gitlab/midgard/kDep/'

# - - - - -
# environment dir - full path to environment folder for project
# - - - - -
environment_dir='/media/dataDrive/Dropbox/gitlab/midgard/environments/'

# - - - - -
# project folder name - must be identical the the name of the folder, under the kDeployer "projects" directory
# - - - - -
project_folder='huginnokmuninn'


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# GENERIC CODE, THAT MOST LIKELY IS IDENTICAL IN BETWEEN PROJECTS, HENCE EDITING THE CODE BELOW SHOULD NOT BE NEEDED
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - - - -
# deploy kDeployer.pl core files to project
# - - - - - - -
source_dir='../../core'
rsync -a $source_dir/kDeployer.pl $target_dir
rsync -a $source_dir/kClosureCompiler.pm $target_dir

# - - - - - - -
# deploy ini file (kDeployer.ini) to project
# - - - - - - -
source_dir='.'
rsync -a $source_dir/kDeployer.ini $target_dir

# - - - - - - -
# deploy environments to project
# - - - - - - -
source_dir='environments/'
rsync -a $source_dir $environment_dir

# - - - - - - -
# deploy exclude files to project
# - - - - - - -
source_dir='.'
rsync -a $source_dir/project_*.txt $target_dir

# - - - - - - -
# deploy build files to project
# - - - - - - -
source_dir='.'
rsync -a $source_dir/*.sh $target_dir --exclude=_deploy.sh
